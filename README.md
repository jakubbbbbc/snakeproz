to build:
- add all files to a source file in an IntelliJ project
- set up various settings if prompted by IntelliJ
- run the main method in SnakeMain.java
- for tests, uncomment the two commented lines in main